const router = require('express').Router()

const db = require('./db')

const table = 'rides'

router.get('/', async (req, res) => {
	const rides = await
		db.getAll(table, req.query.limit)
		.catch( error => console.log(error))

	if (rides.length==0) {
		return res.json([])
	}
	const locs = await
		db.getAll('rides_locations')
		.catch( error => console.log(error))
	
	rides.map( ride => {
		ride.locationsTraveled = locs
			.filter( f => f.id==ride.id )
			.map( loc  => db.location.decode(loc.location))
		ride.fromLocation      = db.location.decode(ride.fromLocation)
		ride.toLocation        = db.location.decode(ride.toLocation)
		ride.currentLocation   = db.location.decode(ride.currentLocation)
	})
	res.json(rides)
})
router.get('/:id', async (req, res) => {
	const rides = await
		db.getSQL(`
			SELECT *
			FROM ${table} B
			LEFT JOIN users U ON B.user = U.id
			LEFT JOIN cars C ON B.car = C.id
			WHERE B.id = ${req.params.id}
		`)
		.catch( error => console.log(error))

	if (rides.length==0) {
		return res.json({})
	}
	const ride = Object.assign(rides[0].B, { user: rides[0].U, car: rides[0].C })

	ride.fromLocation      = db.location.decode(ride.fromLocation)
	ride.toLocation        = db.location.decode(ride.toLocation)
	ride.currentLocation   = db.location.decode(ride.currentLocation)

	db.get('rides_locations', ride.id)
	.then( locs => locs.map( loc => ({ location: db.location.decode(loc.location) })))
	.then( locs => {
		ride.locationsTraveled = locs
		res.json(ride)
	})
})

router.post('/', (req, res) => {
    if (req.body.fromLocation) {
        req.body.fromLocation = db.location.encode(req.body.fromLocation)
    }
    if (req.body.toLocation) {
        req.body.toLocation = db.location.encode(req.body.toLocation)
    }
    if (req.body.currentLocation) {
        req.body.currentLocation = db.location.encode(req.body.currentLocation)
	}
	let locationsTraveled
    if (req.body.locationsTraveled) {
		locationsTraveled = req.body.locationsTraveled
		delete req.body.locationsTraveled
	}
	db.post(table, req.body).then( result => {
		if (result.code && result.code > 0) {
			locationsTraveled.map( loc => db.post('rides_locations', { id: result.code, location: db.location.encode(loc)}))
		}
		res.json(result)
	})
})

router.put('/', (req, res) => {
    if (req.body.fromLocation) {
        req.body.fromLocation = db.location.encode(req.body.fromLocation)
    }
    if (req.body.toLocation) {
        req.body.toLocation = db.location.encode(req.body.toLocation)
    }
	let locationsTraveled
    if (req.body.locationsTraveled) {
		locationsTraveled = req.body.locationsTraveled
		delete req.body.locationsTraveled
		db.del('rides_locations', req.body.id)
	}
    if (req.body.currentLocation) {
        req.body.currentLocation = db.location.encode(req.body.currentLocation)
    }
	db.put(table, req.body).then( result => {
		if (result.code && result.code > 0) {
			locationsTraveled.map( loc => db.post('rides_locations', { id: result.code, location: db.location.encode(loc)}))
		}
		res.json(result)
	})
})

router.delete('/:id', (req, res) => {
	db.del(table, req.params.id).then( result => res.json(result))
})


module.exports = router
