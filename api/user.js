const router = require('express').Router()

const db = require('./db')

const table = 'users'

router.get('/:email', async (req, res) => {
	const nested = await
		db.getSQL(`
			SELECT U.*, S.*
			FROM ${table} U
			LEFT JOIN users_stations X ON U.id = X.userId
			LEFT JOIN stations S ON X.stationId = S.id
			WHERE U.email = '${req.params.email}'
		`)
		.catch( error => console.log(error))
	
	res.json({...nested[0].U, stations: nested.map( m => m.S ) })
})

module.exports = router
