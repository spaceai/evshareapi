const router = require('express').Router()

const db = require('./db')

const table = 'rideSummaries'

router.get('/', (req, res) => {
	db.getAll(table, req.query.limit)
	.then( results => res.json(results))
})
router.get('/:id', (req, res) => {
	db.get(table, req.params.id)
	.then( results => res.json(results[0]))
})

module.exports = router
