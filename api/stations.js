const router = require('express').Router()

const db = require('./db')

const table = 'stations'

router.get('/', (req, res) => {
	db.getAll(table, req.query.limit)
	.then( results => results.map( result => ({...result, location: db.location.decode(result.location) })))
	.then( results => res.json(results))
})
router.get('/:id', (req, res) => {
	db.get(table, req.params.id)
	.then( results => results.map( result => ({...result, location: db.location.decode(result.location) })))
	.then( results => res.json(results[0]))
})

router.post('/', (req, res) => {
	if (req.body.location) {
		req.body.location = db.location.encode(req.body.location)
	}
	db.post(table, req.body).then( result => res.json(result))
})

router.put('/', (req, res) => {
	if (req.body.location) {
		req.body.location = db.location.encode(req.body.location)
	}
	db.put(table, req.body).then( result => res.json(result))
})

router.delete('/:id', (req, res) => {
	db.del(table, req.params.id).then( result => res.json(result))
})

module.exports = router
