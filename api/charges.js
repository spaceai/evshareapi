const router = require('express').Router()

const db = require('./db')

const table = 'charges'

router.get('/', async (req, res) => {
	const charges = await
		db.getAll(table, req.query.limit)
		.catch( error => console.log(error))

	res.json(charges)
})
router.get('/:id', async (req, res) => {
	const charges = await
		db.getSQL(`
			SELECT *
			FROM ${table} C
			LEFT JOIN users U ON C.user = U.id
			LEFT JOIN stations S ON C.station = S.id
			WHERE C.id = ${req.params.id}
		`)
		.catch( error => console.log(error))
	
	if (charges.length) {
		const charge = Object.assign(charges[0].C, { user: charges[0].U, station: charges[0].S })
		res.json(charge)
	}
	else {
		res.json({})
	}
})

router.post('/', (req, res) => {
    if (req.body.station && req.body.station.id) {
        req.body.station = req.body.station.id
    }
    if (req.body.user && req.body.user.id) {
        req.body.user = req.body.user.id
    }
	db.post(table, req.body).then( result => res.json(result))
})

router.put('/', (req, res) => {
    if (req.body.station && req.body.station.id) {
        req.body.station = req.body.station.id
    }
    if (req.body.user && req.body.user.id) {
        req.body.user = req.body.user.id
    }
	db.put(table, req.body).then( result => res.json(result))
})

router.delete('/:id', (req, res) => {
	db.del(table, req.params.id).then( result => res.json(result))
})

module.exports = router
