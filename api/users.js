const router = require('express').Router()

const db = require('./db')

const table = 'users'

router.get('/', (req, res) => {
	db.getAll(table, req.query.limit)
	.then( users => res.json(users))
})
router.get('/:id', async (req, res) => {
	const nested = await
		db.getSQL(`
			SELECT U.*, S.*
			FROM ${table} U
			LEFT JOIN stations S ON U.id = S.userId
			WHERE U.id = ${req.params.id}
		`)
		.catch( error => console.log(error))
	
	res.json({...nested[0].U, stations: nested.map( m => m.S ) })
})

router.post('/', (req, res) => {
	db.post(table, req.body).then( result => res.json(result))
})

router.put('/', (req, res) => {
	db.post(table, req.body).then( result => res.json(result))
})

router.delete('/:id', (req, res) => {
	db.del(table, req.params.id).then( result => res.json(result))
})

module.exports = router
