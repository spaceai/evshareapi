-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Nov 14, 2018 at 11:40 PM
-- Server version: 5.7.24-0ubuntu0.16.04.1-log
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `evshare`
--
CREATE DATABASE IF NOT EXISTS `evshare` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `evshare`;

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` int(10) UNSIGNED NOT NULL,
  `carId` int(10) UNSIGNED NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(10) UNSIGNED NOT NULL,
  `model` varchar(100) NOT NULL,
  `battery` bigint(20) UNSIGNED NOT NULL,
  `location` varchar(30) NOT NULL COMMENT 'lat,lon',
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `charges`
--

CREATE TABLE `charges` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` bigint(20) UNSIGNED NOT NULL,
  `cost` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(100) NOT NULL,
  `userId` int(10) UNSIGNED NOT NULL,
  `stationId` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rides`
--

CREATE TABLE `rides` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fromLocation` varchar(30) NOT NULL COMMENT 'lat,lon',
  `toLocation` varchar(30) NOT NULL COMMENT 'lat,lon',
  `currentLocation` varchar(30) NOT NULL COMMENT 'lat,lon',
  `locationsTraveled` tinyint(1) NOT NULL,
  `userId` int(10) UNSIGNED NOT NULL,
  `carId` int(10) UNSIGNED NOT NULL,
  `status` varchar(100) NOT NULL,
  `distance` bigint(20) UNSIGNED NOT NULL,
  `fromTime` datetime NOT NULL,
  `toTime` datetime NOT NULL,
  `energy` bigint(20) UNSIGNED NOT NULL,
  `cost` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stations`
--

CREATE TABLE `stations` (
  `id` int(10) UNSIGNED NOT NULL,
  `userId` int(10) UNSIGNED NOT NULL,
  `charge` bigint(20) UNSIGNED NOT NULL,
  `maxCharge` bigint(20) UNSIGNED NOT NULL,
  `location` varchar(30) NOT NULL COMMENT 'lat,lon'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stations_users`
--

DROP TABLE `stations_users`;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` char(40) NOT NULL COMMENT 'SHA1',
  `stations` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users` (`userId`),
  ADD KEY `fk_cars` (`carId`);

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `charges`
--
ALTER TABLE `charges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users` (`userId`),
  ADD KEY `fk_stations` (`stationId`);

--
-- Indexes for table `rides`
--
ALTER TABLE `rides`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cars` (`carId`),
  ADD KEY `fk_users` (`userId`);

--
-- Indexes for table `stations`
--
ALTER TABLE `stations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `charges`
--
ALTER TABLE `charges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rides`
--
ALTER TABLE `rides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stations`
--
ALTER TABLE `stations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;



--
-- Table structure for table `rides_locations`
--

CREATE TABLE `rides_locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `rideId` int(10) UNSIGNED NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `locationTraveled` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users_stations`
--

CREATE TABLE `users_stations` (
  `id` int(10) UNSIGNED NOT NULL,
  `stationId` int(10) UNSIGNED NOT NULL,
  `userId` int(10) UNSIGNED NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rides_locations`
--
ALTER TABLE `rides_locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_rides` (`rideId`);

--
-- Indexes for table `users_stations`
--
ALTER TABLE `users_stations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fk_stations` (`stationId`,`userId`),
  ADD KEY `userId` (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rides_locations`
--
ALTER TABLE `rides_locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users_stations`
--
ALTER TABLE `users_stations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
