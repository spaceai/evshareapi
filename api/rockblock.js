const router = require('express').Router()

let logs = []
const fs = require('fs')
const glob = require('glob')
const path = require('path')
// https://github.com/isaacs/node-glob
glob('logs/*.json', (error, files) => {
	if (error) {
		return console.error(error)
	}
	else {
		files
			.map( file => path.resolve('logs', file))
			// .filter( file => ! fs.statSync(file).isDirectory())
			.map( file => logs.push(JSON.parse(fs.readFileSync(file))))
	}
})

router.get('/', (req, res) => {
	res.json(logs)
})

// https://stackoverflow.com/questions/52261494/hex-to-string-string-to-hex-conversion-in-nodejs
const convert = (from, to) => str => Buffer.from(str, from).toString(to)
const utf8ToHex = convert('utf8', 'hex')
const hexToUtf8 = convert('hex', 'utf8')
// hexToUtf8(utf8ToHex('dailyfile.host')) === 'dailyfile.host'

router.post('/', async (req, res) => {
/*
	parser.add_argument('device_type')
	parser.add_argument('serial')
	parser.add_argument('momsn')
	parser.add_argument('transmit_time')
	parser.add_argument('imei')
	parser.add_argument('iridium_latitude')
	parser.add_argument('iridium_longitude')
	parser.add_argument('iridium_cep')
	parser.add_argument('iridium_session_status')
	parser.add_argument('data')
	args = parser.parse_args()

	args['decoded'] = bytearray.fromhex(args['data']).decode(encoding="Latin1")
*/
	let log = req.body
	log.data = hexToUtf8(log.data)
	fs.writeFile(`logs/${log.serial}_${log.transmit_time}.json`, JSON.stringify(log), (error, result) => {
		if (error) console.error(error)

		console.log(result)
	})
	logs.push(log)

	res.send('OK')
})

module.exports = router
