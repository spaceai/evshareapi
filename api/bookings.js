const router = require('express').Router()

const db = require('./db')

const table = 'bookings'

router.get('/', async (req, res) => {
	const bookings = await
		db.getAll(table, req.query.limit)
		.catch( error => console.log(error))

	res.json(bookings)
})
router.get('/:id', async (req, res) => {
	const bookings = await
		db.getSQL(`
			SELECT *
			FROM ${table} B
			LEFT JOIN users U ON B.user = U.id
			LEFT JOIN cars C ON B.car = C.id
			WHERE B.id = ${req.params.id}
		`)
		.catch( error => console.log(error))

	if (bookings.length) {
		const booking = Object.assign(bookings[0].B, { user: bookings[0].U, car: bookings[0].C })
		res.json(booking)
	}
	else {
		res.json({})
	}
})

router.post('/', (req, res) => {
    if (req.body.car && req.body.car.id) {
        req.body.car = req.body.car.id
    }
    if (req.body.user && req.body.user.id) {
        req.body.user = req.body.user.id
    }
	db.post(table, req.body).then( result => res.json(result))
})

router.put('/', (req, res) => {
    if (req.body.car && req.body.car.id) {
        req.body.car = req.body.car.id
    }
    if (req.body.user && req.body.user.id) {
        req.body.user = req.body.user.id
    }
	db.put(table, req.body).then( result => res.json(result))
})

router.delete('/:id', (req, res) => {
	db.del(table, req.params.id).then( result => res.json(result))
})

module.exports = router
