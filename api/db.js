const pool = require('../database')

module.exports = {
	getAll: async (table, limit = 0) => {
		const limited = limit ? `LIMIT ${limit}` : ''
		const results = await
			pool.query(`SELECT * FROM ${table} ${limited}`)
			.catch( error => console.log(error))
		
		return results
	},
	get: async (table, ids) => {
		const results = await
			pool.query(`SELECT * FROM ${table} WHERE id IN (${ids || 0})`)
			.catch( error => console.log(error))
		
		return results
	},
	getSQL: async (sql) => {
		const results = await
			pool.query({ sql: sql, nestTables: true })
			.catch( error => console.log(error))
		
		return results
	},
	getFields: async () => {
		const fields = await
			pool.query(`SHOW FIELDS FROM ${table}`)
			.then( fields => fields.map( field => field.Field ))

		return fields
	},
	post: async (table, body) => {
		if (Object.keys(body).length==0) {
			return { code: -400, message: 'No POST data received' }
		}
		const result = { code: -500, message: 'Inserted id' }

		Object.keys(body).map( k => { if (typeof(body[k])==Object) delete body[k] })

		const { insertId } = await
			pool.query(`INSERT INTO ${table} SET ?`, body)
			.catch( error => console.log(error) || (result.message = error ))
		
		result.code = insertId

		return result
	},
	put: async (table, body) => {
		if (Object.keys(body).length==0) {
			return { code: -400, message: 'No PUT data received' }
		}
		const result = { code: -500, message: 'Changed records' }
		const { changedRows } = await
			pool.query(`UPDATE ${table} SET ?`, body)
			.catch( error => console.log(error) || (result.message = error ))
		
		result.code = changedRows

		return result
	},
	del: async (table, id) => {
		const result = { code: -500, message: 'Deleted records' }
		const { affectedRows } = await
			pool.query(`DELETE FROM ${table} WHERE id = ${id}`)
			.catch( error => console.log(error) || (result.message = error ))
		
		result.code = affectedRows

		return result
	},
	location: {
		encode: (obj) => {
			if (typeof(obj)!=Object) {
				return ''
			}
			const { lat, lon } = obj
			if (lat!=lat) {
				// TODO: check formatting (regex)
			}
			if (lon!=lon) {
				// TODO: check formatting (regex)
			}
			return `${lat},${lon}`
		},
		decode: (str) => {
			if (str=='') {
				return {}
			}
			if (str!=str) {
				// TODO: check formatting (regex)
			}
			const [lat, lon] = str.split(',')
	
			return { lat: lat, lon: lon }
		}	
	}
}
