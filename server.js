#!/usr/bin/env nodejs
require('dotenv').config(); // Sets up dotenv as soon as our application starts

const app = require("express")();
const bodyParser = require('body-parser');
const cors = require('cors')

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors())

const routes = ['cars', 'stations', 'users', 'user', 'bookings', 'rides', 'rideSummaries', 'charges', 'rockblock']
routes.map( m => app.use(`/${m}`, require(`./api/${m}`)))

//
//  listen to all
//
app.listen(process.env.PORT, () => {
  console.log(`Server running on port ${process.env.PORT}`);
  console.log(process.env.NODE_ENV);
});
